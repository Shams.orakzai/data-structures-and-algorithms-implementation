package com.codewithshams;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Array numbers = new Array(3);
        numbers.insert(10);
        numbers.insert(20);
        numbers.insert(30);
        numbers.insert(40);

        // remove at index
        numbers.removeAt(2);
        numbers.removeAt(2);

        // remove last element
        numbers.remove();
        numbers.remove();

        numbers.insert(29);
        numbers.insert(223);
        numbers.insert(44);
        numbers.insert(444);

        // Finding index of a number
        System.out.println(numbers.indexOf(444));

        // Finding max value in array
        System.out.println(numbers.max());

        numbers.print();
        numbers.insertAt(2,94);
        numbers.insertAt(3,50055);
        numbers.insertAt(2,390);
        numbers.print_in_array_form();

        System.out.println(Arrays.toString(numbers.reverse()));
    }
}
