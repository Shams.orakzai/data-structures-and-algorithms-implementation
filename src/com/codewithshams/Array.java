package com.codewithshams;

import java.util.Arrays;

public class Array {
    private int[] items;
    private int count;
    private int max_number;

    public Array(int length) {
        items = new int[length];
    }

    public void insert(int item) {
        if (items.length == count){
            int[] newItems = new int[count * 2];

            for (int i = 0; i < count; i++)
                newItems[i] = items[i];

            items = newItems;
        }
        items[count++] = item;
    }

    public void insertAt(int index, int item) {
        if (items.length == count) {
            int[] newItems = new int[count * 2];

            for (int i = 0; i < count; i++)
                newItems[i] = items[i];
            items = newItems;
        }
        if (index > count || index < 0) {
            throw new IllegalArgumentException();
        }
        for (int i = count; i > index; i--) {
            items[i] = items[i - 1];
        }
        items[index] = item;
        count++;
    }

    public void removeAt(int index) {
        //step: 1 --> Validate the index
        if (index < 0 || index >= count)
            throw new IllegalArgumentException();

        //step: 2 --> shift items to left to fill the hole
        for (int i = index; i < count; i++)
            items[i] = items[i + 1];

        count--;
    }

    public void remove() {
        if (count > 0){
            count--;
        }
        else System.out.println("No element in array to remove.");
    }

    public int indexOf(int item) {
        // Step: 1 --> start from 0 index and compare item with every element
        for (int i = 0; i < count; i++)
            if (items[i] == item){
                return i;
            }
        return -1;
    }

    public int max() {
        for (int i = 0; i < count; i++)
            if (items[i] > max_number) {
                max_number = items[i];
            }
        return max_number;
    }

    public int[] reverse() {
        int[] temp = new int[count];
        int temp_index = count - 1;
        for (int i = 0; i < count; i++) {
            temp[temp_index] = items[i];
            temp_index = temp_index - 1;
        }
        return temp;
    }

    public void print() {
        for (int i = 0; i < count; i++)
            System.out.println(items[i]);
    }

    public void print_in_array_form() {
        int[] temp = new int[count];
        for (int i = 0; i < count; i++)
            temp[i] = items[i];
        System.out.println(Arrays.toString(temp));
    }
}
